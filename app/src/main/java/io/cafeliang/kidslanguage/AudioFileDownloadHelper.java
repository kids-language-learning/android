package io.cafeliang.kidslanguage;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import org.chromium.net.CronetEngine;
import org.chromium.net.CronetException;
import org.chromium.net.UrlRequest;
import org.chromium.net.UrlResponseInfo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AudioFileDownloadHelper {
    private static final String TAG = AudioFileDownloadHelper.class.getSimpleName();

    private static final String DOWNLOAD_HTTP_METHOD = "GET";

    private static AudioFileDownloadHelper instance;

    private AudioFileDownloadHelper() { }

    public static AudioFileDownloadHelper getInstance() {
        if (instance == null) {
            return new AudioFileDownloadHelper();
        }

        return instance;
    }

    public void downloadFile(Context context, CronetEngine cronetEngine, Uri downloadUri, AudioFileDownloadListener listener) {
        UrlRequest request = buildRequest(context, cronetEngine, downloadUri, listener);
        request.start();
    }

    public UrlRequest buildRequest(final Context context, CronetEngine cronetEngine, Uri downloadUri, final AudioFileDownloadListener downloadListener) {
        Executor executor = Executors.newSingleThreadExecutor();

        UrlRequest.Builder requestBuilder = cronetEngine.newUrlRequestBuilder(
                downloadUri.toString(),
                new UrlRequest.Callback() {
                    FileChannel outputChannel;
                    String localPath;
                    File downloadedFile;

                    @Override
                    public void onRedirectReceived(UrlRequest request, UrlResponseInfo info, String newLocationUrl) throws Exception {
                        Log.i(TAG, "onRedirectReceived: ");
                    }

                    @Override
                    public void onResponseStarted(UrlRequest request, UrlResponseInfo info) throws Exception {
                        Log.i(TAG, "onResponseStarted: ");
                        Log.d(TAG, "status code: " + info.getHttpStatusCode());

                        int httpStatusCode = info.getHttpStatusCode();
                        switch (httpStatusCode) {
                            case 200:
                                localPath = context.getFilesDir() + "/audio.mp3";
                                downloadedFile = new File(localPath);

                                try {
                                    // Check if parent folders exists
                                    File fileFolder = downloadedFile.getParentFile();
                                    if(!fileFolder.exists()) {
                                        if(!fileFolder.mkdirs()) {
                                            String message = "Failed to create directory "
                                                    + fileFolder.getAbsolutePath();
                                            downloadListener.onDownloadFailed(new IOException(message));
                                        }
                                    }
                                    outputChannel = new FileOutputStream(localPath).getChannel();
                                    request.read(ByteBuffer.allocateDirect((int) info.getReceivedByteCount()));
                                } catch (IOException e) {
                                    downloadListener.onDownloadFailed(e);
                                }
                                break;
                            case 403:
                                String errorMessage = "HTTP status code: " + httpStatusCode +
                                        ". Does the user have read access to the GCS bucket?";
                                downloadListener.onDownloadFailed(new Exception(errorMessage));
                                break;
                            default:
                                errorMessage = "Unexpected HTTP status code: " + httpStatusCode;
                                downloadListener.onDownloadFailed(new Exception(errorMessage));
                                break;
                        }
                    }

                    @Override
                    public void onReadCompleted(UrlRequest request, UrlResponseInfo info, ByteBuffer byteBuffer) throws Exception {
                        Log.i(TAG, "onReadCompleted: ");
                        request.read(ByteBuffer.allocateDirect((int)info.getReceivedByteCount()));
                        byteBuffer.flip();
                        try {
                            outputChannel.write(byteBuffer);
                        } catch (IOException e) {
                            downloadListener.onDownloadFailed(e);
                        }
                    }

                    @Override
                    public void onSucceeded(UrlRequest request, UrlResponseInfo info) {
                        Log.i(TAG, "onSucceeded: ");
                        int httpStatusCode = info.getHttpStatusCode();
                        if(httpStatusCode == 200) {
                            try {
                                outputChannel.close();
                                File downloadedFile = new File(localPath);
                                downloadListener.onDownloadSucceeded(downloadedFile);
                            } catch (IOException e) {
                                downloadListener.onDownloadFailed(e);
                            }
                        }
                    }

                    @Override
                    public void onFailed(UrlRequest request, UrlResponseInfo info, CronetException error) {
                        Log.d(TAG, "onFailed: ");
                        downloadListener.onDownloadFailed(error);
                    }
                }
        , executor)
        .setHttpMethod(DOWNLOAD_HTTP_METHOD);

        return requestBuilder.build();
    }

    public interface AudioFileDownloadListener {
        void onDownloadSucceeded(File file);
        void onDownloadFailed(Exception e);
    }
}
