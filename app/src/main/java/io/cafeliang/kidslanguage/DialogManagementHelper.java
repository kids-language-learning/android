package io.cafeliang.kidslanguage;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import io.cafeliang.kidslanguage.dialogmanager.DialogRequest;
import io.cafeliang.kidslanguage.dialogmanager.DialogResponse;
import io.cafeliang.kidslanguage.dialogmanager.DialogResponsesAPI;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DialogManagementHelper {
    private static final String TAG = DialogManagementHelper.class.getSimpleName();
    
    private static final String BASE_URL = "https://loqkegxfyj.execute-api.us-east-1.amazonaws.com/beta/";
    private static DialogManagementHelper instance;
    private DialogResponsesAPI dialogResponseAPI;


    private DialogManagementHelper() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        dialogResponseAPI = retrofit.create(DialogResponsesAPI.class);

    }

    public static DialogManagementHelper getInstance() {
        if (instance == null) {
            instance = new DialogManagementHelper();
        }

        return instance;
    }

    public void getDialogResponse(final DialogRequest request, final DialogResponseListener dialogResponseListener) {
        Log.d(TAG, "getDialogResponse: ");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Call<DialogResponse> call = dialogResponseAPI.getDialogResponse(request);
                try {
                    Response<DialogResponse> response = call.execute();

                    if (response.code() == 200) {
                        Log.d(TAG, "response succeeded: ");
                        DialogResponse responseBody = response.body();
                        dialogResponseListener.onResponseSucceeded(responseBody);
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Fail to get dialog response: " + e.getLocalizedMessage());
                    dialogResponseListener.onResponseFailed(e);
                }
            }
        }).start();
    }

    public interface DialogResponseListener {
        void onResponseSucceeded(DialogResponse response);
        void onResponseFailed(Exception e);
    }
}
