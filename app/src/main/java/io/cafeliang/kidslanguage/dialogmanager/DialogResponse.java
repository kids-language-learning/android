package io.cafeliang.kidslanguage.dialogmanager;

import com.google.gson.annotations.SerializedName;

public class DialogResponse {

    Word word;
    String responseText;

    @SerializedName("audio_uri")
    String audioUri;
    DisplayResponse display;
    int reward;
    private ImageResponse reward_images;


    public Word getWord() {
        return word;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public String getAudioUri() {
        return audioUri;
    }

    public void setAudioUri(String audioUri) {
        this.audioUri = audioUri;
    }

    public DisplayResponse getDisplay() {
        return display;
    }

    public void setDisplay(DisplayResponse display) {
        this.display = display;
    }

    public int getReward() {
        return reward;
    }

    public ImageResponse getRewardImage() {
        return reward_images;
    }
}
