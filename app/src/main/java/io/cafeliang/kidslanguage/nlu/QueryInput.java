package io.cafeliang.kidslanguage.nlu;


class QueryInput {
    QueryInputText text;

    public QueryInput(String text, String languageCode) {
        this.text = new QueryInputText(text, languageCode);
    }

    public QueryInputText getText() {
        return text;
    }

    public void setText(QueryInputText text) {
        this.text = text;
    }
}

