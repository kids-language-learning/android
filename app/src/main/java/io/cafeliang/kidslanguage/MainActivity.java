package io.cafeliang.kidslanguage;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import org.chromium.net.CronetEngine;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import io.cafeliang.kidslanguage.dialogmanager.DialogRequest;
import io.cafeliang.kidslanguage.dialogmanager.DialogResponse;
import io.cafeliang.kidslanguage.dialogmanager.ImageResponse;
import io.cafeliang.kidslanguage.dialogmanager.Word;
import io.cafeliang.kidslanguage.nlu.NluRequest;
import io.cafeliang.kidslanguage.nlu.NluResponse;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = MainActivity.class.getSimpleName();
//    private static final int REQUEST_OPEN_DOCUMENT = 123;

    // google asr
    private SpeechService mSpeechService;

    // network
    private CronetEngine cronetEngine;

    TextView textView;
    ImageButton microphoneButton;
    private ImageView imageView;
    private ProgressBar spinner;

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private MyAdapter adapter;


    private Intent videoRecordIntent;

    private String sessionId;
    private List<Word> animals;
    private String currentText;

    private String raw_file_path;

    private SimpleExoPlayer player;
    private ComponentListener componentListener;
    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();
    private boolean playWhenReady = true;
    private int currentWindow;
    private long playbackPosition;

    private VoiceRecorder mVoiceRecorder;

    boolean isExecutingTouchInputTask = false;
    boolean isExecutingVoiceRequestTask = false;
    private long playerEndedTime;


    private final VoiceRecorder.Callback mVoiceCallback = new VoiceRecorder.Callback() {

        @Override
        public void onVoiceStart() {
            Log.d(TAG, "onVoiceStart: ");
            Log.d(TAG, "player state: " + player.getPlaybackState());

            if (mSpeechService != null) {
//                mSpeechService.startRecognizing(mVoiceRecorder.getSampleRate());
                String file_path = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());
                mVoiceRecorder.setFilePath(file_path);
            }

            if (isCloseToPlayerEndedTime()) {
                return;
            }

            if (shouldNotSaveVoiceRecording()) {
                mVoiceRecorder.setStopListeningAfterNoHearing(true);
                mVoiceRecorder.setShouldSaveRecording(false);
                return;
            } else {
                mVoiceRecorder.setStopListeningAfterNoHearing(false);
                mVoiceRecorder.setShouldSaveRecording(true);
            }

            showStatus(MicrophoneStatus.HearingVoice);
        }

        @Override
        public void onVoice(byte[] data, int size) {
            Log.d(TAG, "onVoice: ");
            Log.d(TAG, "player state: " + player.getPlaybackState());

            if (!mVoiceRecorder.getShouldSaveRecording())
                return;

            if (isCloseToPlayerEndedTime()) {
                return;
            }

            if (shouldNotSaveVoiceRecording()){
                mVoiceRecorder.setStopListeningAfterNoHearing(true);

                Log.d(TAG, "shouldNotSaveVoiceRecording: ");
                return;
            }

            if (isCloseToPlayerEndedTime()) {
                return;
            }

            mVoiceRecorder.setStopListeningAfterNoHearing(false);
            showStatus(MicrophoneStatus.HearingVoice);

//            if (mSpeechService != null) {
//                mSpeechService.recognize(data, size);
//            }
        }

        @Override
        public void onVoiceEnd() {
            Log.d(TAG, "onVoiceEnd: ");
            if (!mVoiceRecorder.getShouldSaveRecording())
                return;

            showStatus(MicrophoneStatus.NotHearingVoice);

            if (isCloseToPlayerEndedTime()) {
                return;
            }

            if (mSpeechService != null) {
//                mSpeechService.finishRecognizing();
                try {
                    if (mVoiceRecorder != null && mVoiceRecorder.getRecordingFile() != null) {
                        FileInputStream fileInputStream = new FileInputStream(mVoiceRecorder.getRecordingFile());
                        mSpeechService.recognizeInputStream(fileInputStream);

                        runOnUiThread(() -> spinner.setVisibility(View.VISIBLE));
                    }

                } catch (FileNotFoundException e) {
                    Log.e(TAG, e.getLocalizedMessage());
                }
            }
        }
    };
    private long appStartTime;

    private long touchEventTime;
    private final int playerDelayTime = 1000;
    private boolean isCloseToPlayerEndedTime() {
        Log.d(TAG, "playerEndedTime: " + playerEndedTime);

        Log.d(TAG, "touchEventTime: " + touchEventTime);

        long currentTime = System.currentTimeMillis();
        Log.d(TAG, "System.currentTimeMillis(): " + currentTime);

        boolean isCloseToPreviousPlayerEndedTime =
                currentTime - playerEndedTime < playerDelayTime ||
                currentTime - appStartTime < 3000 ||
                currentTime - touchEventTime < 7000;

        Log.d(TAG, "isCloseToPlayerEndedTime: " + isCloseToPreviousPlayerEndedTime);

        return isCloseToPreviousPlayerEndedTime;
    }



    private boolean shouldNotSaveVoiceRecording() {
        Log.d(TAG, "isExecutingVoiceRequestTask: " + isExecutingVoiceRequestTask);
        Log.d(TAG, "isExecutingTouchInputTask: " + isExecutingTouchInputTask);
        boolean shouldNotSaveVoiceRecording =
                isExecutingVoiceRequestTask ||
                isExecutingTouchInputTask ||
                (player.getPlaybackState() != Player.STATE_IDLE &&
                    player.getPlaybackState() != Player.STATE_ENDED);
        Log.d(TAG, "shouldNotSaveVoiceRecording: " + shouldNotSaveVoiceRecording);
        return shouldNotSaveVoiceRecording;
    }

    static enum MicrophoneStatus {
        Mute,
        HearingVoice,
        NotHearingVoice,
    }

    private void showStatus(MicrophoneStatus status) {

        if (status == MicrophoneStatus.HearingVoice) {
//            microphoneButton.setImageDrawable(getDrawable(R.drawable.ic_baseline_mic_24px));
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "showStatus: hearingVoice" );
                    microphoneButton.setBackgroundColor(getColor(android.R.color.white));
                    microphoneButton.setImageDrawable(getDrawable(R.drawable.icons8_mic_full));
                }
            });

        } else if (status == MicrophoneStatus.NotHearingVoice) {
            Log.d(TAG, "showStatus: not hearing voice" );

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "showStatus: false" );
                    microphoneButton.setBackgroundColor(getColor(android.R.color.white));
                    microphoneButton.setImageDrawable(getDrawable(R.drawable.icons8_mic_none));
                }
            });
        } else if (status == MicrophoneStatus.Mute) {
            Log.d(TAG, "showStatus: mute" );

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    microphoneButton.setBackgroundColor(getColor(android.R.color.darker_gray));
                }
            });
        }

    }

    // [begin] Google STT

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder binder) {
            Log.d(TAG, "onServiceConnected: ");
            mSpeechService = SpeechService.from(binder);
            mSpeechService.addListener(mSpeechServiceListener);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mSpeechService = null;
        }

    };

    private final SpeechService.Listener mSpeechServiceListener = new SpeechService.Listener() {
        @Override
        public void onSpeechRecognized(String languageCode, String text, boolean isFinal) {
            Log.d(TAG, "on Speech recognized: [" + languageCode + "] " + text);
//        runOnUiThread(() -> imageView.setText(text));

            Log.d(TAG, "isExecutingVoiceRequestTask: " + isExecutingVoiceRequestTask);
            Log.d(TAG, "isExecutingTouchInputTask: " + isExecutingTouchInputTask);

            runOnUiThread(() -> spinner.setVisibility(View.INVISIBLE));

            if (isExecutingVoiceRequestTask || isExecutingTouchInputTask)
                return;

            isExecutingVoiceRequestTask = true;


            NLUHelper.getInstance().setToken(getGoogleToken());
            NLUHelper.getInstance().detectIntent(
                    new NluRequest(text.toLowerCase(), "en-US"),
                    new NLUHelper.NluListener() {

                        @Override
                        public void onResponseSucceeded(NluResponse response) {
                            Log.d(TAG, "detect intent succeeded: ");
                            String intent = response.getQueryResult().getIntent();
                            Log.d(TAG, "intent: " + intent);

                            Map<String, String> params = response.getQueryResult().getParameters();

                            if (intent.equals("LaunchIntent")) {
                                sessionId = UUID.randomUUID().toString();
                            }

                            params.put("session_id", sessionId);
                            Log.d(TAG, "params: " + params.toString());

                            getDialogResponse(intent, params);
                        }

                        @Override
                        public void onResponseFailed(Exception e) {
                            Log.d(TAG, "detectIntent failed: " + e.getLocalizedMessage());
                        }
                    });
        }

        @Override
        public void onError(Throwable t) {
            Log.e(TAG, "onError: ", t);
            isExecutingVoiceRequestTask = false;
            runOnUiThread(() -> spinner.setVisibility(View.INVISIBLE));
        }

        @Override
        public void onCompleted() {
            Log.d(TAG, "onRecognition Completed: ");
            isExecutingVoiceRequestTask = false;
            runOnUiThread(() -> spinner.setVisibility(View.INVISIBLE));
        }
    };



    // [end] Google STT

    private void getDialogResponse(String intent, Map<String, String> params) {
        DialogManagementHelper.getInstance().getDialogResponse(
                new DialogRequest(intent, params),
                new DialogManagementHelper.DialogResponseListener() {
                    @Override
                    public void onResponseSucceeded(DialogResponse response) {
                        String responseText = response.getResponseText();
                        Log.d(TAG, "response text: " + responseText);
                        Log.d(TAG,  "audio url: " + response.getAudioUri());
                        Log.d(TAG, "reward count: " + response.getReward());

                        if (response.getWord() != null) {
                            for (int i = 0; i < response.getReward(); i++) {
                                animals.add(response.getWord());
                            }
                            runOnUiThread(() -> adapter.notifyDataSetChanged());
                        }

                        if (response.getDisplay() != null) {
                            Log.d(TAG, "display text: " + response.getDisplay().getText());
                            Log.d(TAG, "display image 1 key: " + response.getDisplay().getImages().get(0).getKey());
                        }


//                        if (response.getAudioUri() != null) {
//                            Log.d(TAG, "response audio: " + response.getAudioUri());
//                            Uri audioUri = Uri.parse(response.getAudioUri());
//                            playAudio(audioUri);
//                        }
                        if (response.getAudioUri() != null) {
                            Log.d(TAG, "response audio: " + response.getAudioUri());
                            String filename = response.getAudioUri();
//                            if (filename.equals("https://kids-language-learning.s3.amazonaws.com/sentence/duck.mp3")) {
//                                filename="duck";
//                            }
//                            if (filename.equals("dog")) {
//                            } else {
//                            String path = Environment.getExternalStorageDirectory().getPath()+ "/kidslanguage/"+ filename +".mp3";
//                            Log.d(TAG, "file: " + path);
//                            Uri uri = Uri.parse(path);
//                            int resId = getResId(response.getAudioUri(), R.raw.class);
//                                playAudio(uri);
                            Uri uri = Uri.parse(response.getAudioUri());
                            playAudioWithExoPlayer(uri);
//                            }
                        }

                        if (response.getDisplay() != null) {
                            // TODO: 8/13/19
                            String key = response.getDisplay().getImages().get(0).getKey();
                            Log.d(TAG, "Key: " + key);

                            int delayTime = 0;
                            ImageResponse rewardImage = response.getRewardImage();
                            if (rewardImage != null) {
                                delayTime = rewardImage.getDelayTime();
                                Log.d(TAG, "reward image key: " + rewardImage.getKey());

                                    runOnUiThread(() -> {
                                        int resId = getRewardImageRes(rewardImage.getKey());

                                        if (resId != -1) {
                                            Glide.with(MainActivity.this)
                                                    .asGif()
                                                    .load(resId)
                                                    .into(imageView);
                                        }
                                    });
                            }

                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.postDelayed(() -> {
                                if (response.getDisplay() != null) {
                                    currentText = response.getDisplay().getText();
                                    textView.setText(currentText);
                                    Log.d(TAG, "text: " + response.getDisplay().getText());
                                } else {
                                    currentText = "";
                                    textView.setText(currentText);
                                }

                                Log.d(TAG, "current text: " + currentText);

                                if (response.getWord() != null)
                                    imageView.setContentDescription(response.getWord().getES());

                                if (key == null)
                                    return;

                                int imageRes = ImageResUtils.getNormalImageRes(key);
                                Glide.with(MainActivity.this)
                                        .asBitmap()
                                        .load(imageRes)
                                        .into(imageView);
//                                if (key.equals("duck")) {
//                                    imageView.setImageResource(R.drawable.duck);
//                                } else if (key.equals("rabbit")) {
//                                    Glide.with(MainActivity.this)
//                                            .asBitmap()
//                                            .load(R.raw.rabbit)
//                                            .into(imageView);
////                                        imageView.setImageResource(R.raw.rabbit);
////                                                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
//                                } else if (key.equals("fish")) {
//                                    Glide.with(MainActivity.this)
//                                            .asBitmap()
//                                            .load(R.raw.fish)
//                                            .into(imageView);
//                                } else if (key.equals("sheep")) {
//                                    imageView.setImageResource(R.drawable.sheep);
//
////                                    Glide.with(MainActivity.this)
////                                            .asBitmap()
////                                            .load(R.raw.rabbit)
////                                            .into(imageView);
//                                } else if (key.equals("horse")) {
//                                    Glide.with(MainActivity.this)
//                                            .asBitmap()
//                                            .load(R.raw.horse)
//                                            .into(imageView);
//                                } else if (key.equals("dog")) {
//                                    Glide.with(MainActivity.this)
//                                            .asBitmap()
//                                            .load(R.raw.dog)
//                                            .into(imageView);
//                                }
                            }, delayTime);
                        }
                    }

                    @Override
                    public void onResponseFailed(Exception e) {
                        Log.d(TAG, "onResponseFailed: " + e.getLocalizedMessage());
                    }
                });
    }

    private int getRewardImageRes(String imageKey){
        if (imageKey.equals("rabbit")) {
            return R.raw.rabbit;
        } else if (imageKey.equals("fish")) {
            return R.raw.fish;
        } else if (imageKey.equals("horse")) {
            return R.raw.horse;
        }
        return -1;
    }


    private int getRewardCount(DialogResponse response) {
        return response.getReward();
    }

    private String getGoogleToken() {
        // TODO: 8/13/19 Use better way to get token
        final SharedPreferences prefs =
                getSharedPreferences("SpeechService", Context.MODE_PRIVATE);

        return prefs.getString("access_token_value", null);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.response_text);

        imageView = findViewById(R.id.image);
        imageView.setOnClickListener(view -> {
            Log.d(TAG, "onClick animal");

            touchEventTime = System.currentTimeMillis();
            Log.d(TAG, "set toucheventtime: " + touchEventTime);

//            stopVoiceRecorder();

//            String path = Environment.getExternalStorageDirectory().getPath()+ "/kidslanguage/"+ currentText +".mp3";
//            playAudio(Uri.parse(path));

            Log.d(TAG, "is fetching: " + isExecutingTouchInputTask);
//            if (isExecutingTouchInputTask)
//                return;

            isExecutingTouchInputTask = true;
            Log.d(TAG, "isExecutingTouchInputTask: " + true);

            imageView.setClickable(false);
            Log.d(TAG, "imageView: onclickable");

            getDialogResponse(
                    "repeat",
                    new HashMap<String, String>() {{
                        put("session_id", sessionId);
                        put("repeat-word", currentText);
                    }});
        });

        microphoneButton = findViewById(R.id.microphone_button);
        microphoneButton.setOnClickListener(this);

        spinner = findViewById(R.id.progressBar);

        Button startButton = findViewById(R.id.start_button);
        startButton.setOnClickListener(this);

        Button yesButton = findViewById(R.id.yes_button);
        yesButton.setOnClickListener(this);

        Button correctAnsButton = findViewById(R.id.correct_button);
        correctAnsButton.setOnClickListener(this);

        Button wrongAnsButton = findViewById(R.id.wrong_button);
        wrongAnsButton.setOnClickListener(this);


        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        animals = new ArrayList<>();
        adapter = new MyAdapter(animals);
        recyclerView.setAdapter(adapter);

        adapter.notifyDataSetChanged();

        componentListener = new ComponentListener();

//        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
//        intent.setType("*/*");
//        startActivityForResult(intent, REQUEST_OPEN_DOCUMENT);
//
//        StorageManager sm = (StorageManager)getSystemService(Context.STORAGE_SERVICE);
//        StorageVolume volume = sm.getPrimaryStorageVolume();
    }

    private int getAudioResource(String key) {
        if (key.equals("duck")) {
            return R.raw.duck;
        }
        return -1;
    }


    @Override
    protected void onStart() {
        super.onStart();

        // Prepare Cloud Speech API
        bindService(new Intent(this, SpeechService.class), mServiceConnection, BIND_AUTO_CREATE);

        startVideoRecording();

        startVoiceRecorder();

        initializePlayer();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStop() {


        stopRecording();

        stopVoiceRecorder();

        releasePlayer();

        super.onStop();
    }

    private void startVoiceRecorder() {
        if (RecordingHelper.getInstance().hasRequiredPermissions(getApplicationContext())) {

            Log.d(TAG, "startVoiceRecorder: ");
            if (mVoiceRecorder != null) {
                mVoiceRecorder.stop();
            }

            if (mVoiceRecorder == null) {
                Log.d(TAG, "create new voice recorder: ");
                mVoiceRecorder = new VoiceRecorder(mVoiceCallback);
            }

            // Hacky way to control microphone UI
            appStartTime = System.currentTimeMillis();

            mVoiceRecorder.start();
        } else {
            RecordingHelper.getInstance().requestRequiredPermissions(this);
        }
    }

    private void stopVoiceRecorder() {
        if (mVoiceRecorder != null) {
            Log.d(TAG, "stopVoiceRecorder: begin");
            mVoiceRecorder.stop();
            mVoiceRecorder = null;
            Log.d(TAG, "stopVoiceRecorder: finish");
        }
    }


    private void playAudioWithExoPlayer(Uri audioUri) {
        Log.d(TAG, "playAudioWithExoPlayer: ");
//        stopVoiceRecorder();

        MediaSource mediaSource = buildMediaSource(audioUri);

        Log.d(TAG, "playAudioWithExoPlayer: prepare");
        player.prepare(mediaSource, true, false);
    }


    MediaPlayer mediaPlayer;

//    private void playAudio(Uri audioUri) {
//        Log.d(TAG, "playAudio: " + String.valueOf(audioUri));
//        stopVoiceRecorder();
//        runOnUiThread(() -> imageView.setClickable(false));
//
//        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
//            mediaPlayer.stop();
//        }
//
//        isExecutingTouchInputTask = true;
//        mediaPlayer = new MediaPlayer();
//        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//        try {
//            Log.d(TAG, "media player setDataSource: ");
//
//            mediaPlayer.setDataSource(String.valueOf(audioUri));
//            Log.d(TAG, "media player prepare: ");
//            mediaPlayer.prepare();
//
//            Log.d(TAG, "media player start: ");
//            mediaPlayer.start();
//        } catch (IOException e) {
//            Log.e(TAG, "fail to play audio: ", e);
//            isExecutingTouchInputTask = false;
//
//            runOnUiThread(() -> imageView.setClickable(true));
//        }
//
//        mediaPlayer.setOnCompletionListener(mediaPlayer1 -> {
//            Log.d(TAG, "media play completed");
//            isExecutingTouchInputTask = false;
//            runOnUiThread(() -> imageView.setClickable(true));
//
//            startVoiceRecorder();
//        });
//
////        AudioFileDownloadHelper.getInstance().downloadFile(
////                getApplicationContext(), getCronetEngine(), audioUri, new AudioFileDownloadHelper.AudioFileDownloadListener() {
////                    @Override
////                    public void onDownloadSucceeded(File file) {
////                        Log.d(TAG, "onDownloadSucceeded: ");
////                        isExecutingTouchInputTask = false;
////
////                        mediaPlayer = MediaPlayer.create(
////                                getApplicationContext(), Uri.fromFile(file));
////
////                        mediaPlayer.setOnCompletionListener(mediaPlayer1 -> {
////                            Log.d(TAG, "media play completed");
////
////                            startVoiceRecorder();
////                            imageView.setClickable(true);
////                        });
////
////                        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
////                            mediaPlayer.stop();
////                        }
////
////                        stopVoiceRecorder();
////
////                        mediaPlayer.start();
////                        Log.d(TAG, "media play started: ");
////                    }
////
////                    @Override
////                    public void onDownloadFailed(Exception e) {
////                        isExecutingTouchInputTask = false;
////                    }
////                });
//    }

//    private void playAudio(String filepath) {
//        stopVoiceRecorder();
//        runOnUiThread(() -> imageView.setClickable(false));
//
//        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
//            mediaPlayer.stop();
//        }
//
//        isExecutingTouchInputTask = true;
//        mediaPlayer = new MediaPlayer();
//        try {
//            mediaPlayer.setDataSource();
//            mediaPlayer.prepare();
//            mediaPlayer.start();
//        } catch (IOException e) {
//            isExecutingTouchInputTask = false;
//
//            runOnUiThread(() -> imageView.setClickable(true));
//
//            e.printStackTrace();
//        }
//
//        mediaPlayer.setOnCompletionListener(mediaPlayer1 -> {
//            Log.d(TAG, "media play completed");
//            isExecutingTouchInputTask = false;
//            runOnUiThread(() -> imageView.setClickable(true));
//
//            startVoiceRecorder();
//        });
//
////        AudioFileDownloadHelper.getInstance().downloadFile(
////                getApplicationContext(), getCronetEngine(), audioUri, new AudioFileDownloadHelper.AudioFileDownloadListener() {
////                    @Override
////                    public void onDownloadSucceeded(File file) {
////                        Log.d(TAG, "onDownloadSucceeded: ");
////                        isExecutingTouchInputTask = false;
////
////                        mediaPlayer = MediaPlayer.create(
////                                getApplicationContext(), Uri.fromFile(file));
////
////                        mediaPlayer.setOnCompletionListener(mediaPlayer1 -> {
////                            Log.d(TAG, "media play completed");
////
////                            startVoiceRecorder();
////                            imageView.setClickable(true);
////                        });
////
////                        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
////                            mediaPlayer.stop();
////                        }
////
////                        stopVoiceRecorder();
////
////                        mediaPlayer.start();
////                        Log.d(TAG, "media play started: ");
////                    }
////
////                    @Override
////                    public void onDownloadFailed(Exception e) {
////                        isExecutingTouchInputTask = false;
////                    }
////                });
//    }


    private void playAudio(int res) {
        Log.d(TAG, "playAudio: res = " + res);
        Log.d(TAG, "playAudio: R.raw.intro=" + R.raw.intro);
        stopVoiceRecorder();
//        runOnUiThread(() -> imageView.setClickable(false));

        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }

//        isExecutingTouchInputTask = true;
        mediaPlayer = MediaPlayer.create(this, res);
        mediaPlayer.start();

        mediaPlayer.setOnCompletionListener(mediaPlayer1 -> {
            Log.d(TAG, "media play completed");
//            isExecutingTouchInputTask = false;
//            runOnUiThread(() -> imageView.setClickable(true));

//            startVoiceRecorder();
        });
    }


    private void initializePlayer() {
        if (player == null) {
            // a factory to create an AdaptiveVideoTrackSelection
            TrackSelection.Factory adaptiveTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(BANDWIDTH_METER);
            // using a DefaultTrackSelector with an adaptive video selection factory
            player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(this),
                                                        new DefaultTrackSelector(adaptiveTrackSelectionFactory), new DefaultLoadControl());
            player.addListener(componentListener);
            player.addAudioDebugListener(componentListener);

            player.setPlayWhenReady(playWhenReady);
//            player.seekTo(currentWindow, playbackPosition);
        }
//        MediaSource mediaSource = buildMediaSource(Uri.parse(getString(R.string.media_url_mp3)));
    }


    private void releasePlayer() {
        if (player != null) {
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
            player.removeListener(componentListener);
            player.removeAudioDebugListener(componentListener);
            player.release();
            player = null;
        }
    }

    private MediaSource buildMediaSource(Uri uri) {
        Log.d(TAG, "buildMediaSource: start");
        DataSource.Factory dataSourceFactory =
                new DefaultHttpDataSourceFactory(Util.getUserAgent(this, "app-name"));
// Create a progressive media source pointing to a stream uri.
        MediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri);
        Log.d(TAG, "buildMediaSource: finished");

        return mediaSource;
//        DataSource.Factory manifestDataSourceFactory = new DefaultHttpDataSourceFactory("ua");
//        DashChunkSource.Factory dashChunkSourceFactory = new DefaultDashChunkSource.Factory(
//                new DefaultHttpDataSourceFactory("ua", BANDWIDTH_METER));
//        return new DashMediaSource.Factory(dashChunkSourceFactory, manifestDataSourceFactory)
//                .createMediaSource(uri);
    }


    private class ComponentListener extends Player.DefaultEventListener implements AudioRendererEventListener {

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            String stateString;
            switch (playbackState) {
                case Player.STATE_IDLE:
                    stateString = "ExoPlayer.STATE_IDLE      -";
                    break;
                case Player.STATE_BUFFERING:
                    stateString = "ExoPlayer.STATE_BUFFERING -";
                    break;
                case Player.STATE_READY:
                    stateString = "ExoPlayer.STATE_READY     -";

                    showStatus(MicrophoneStatus.Mute);
                    break;
                case Player.STATE_ENDED:
                    stateString = "ExoPlayer.STATE_ENDED     -";

                    playerEndedTime = System.currentTimeMillis();

                    isExecutingVoiceRequestTask = false;
                    isExecutingTouchInputTask = false;

                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(() -> showStatus(MicrophoneStatus.NotHearingVoice), playerDelayTime);

                    mVoiceRecorder.setStopListeningAfterNoHearing(false);
                    mVoiceRecorder.setShouldSaveRecording(true);


                    runOnUiThread(() -> imageView.setClickable(true));
                    Log.d(TAG, "imageView clickable ");
//                    startVoiceRecorder();

                    break;
                default:
                    stateString = "UNKNOWN_STATE             -";
                    break;
            }
            Log.d(TAG, "changed state to " + stateString + " playWhenReady: " + playWhenReady);
        }

        // Implementing AudioRendererEventListener.

        @Override
        public void onAudioEnabled(DecoderCounters counters) {
            // Do nothing.
            Log.d(TAG, "onAudioEnabled: ");
        }

        @Override
        public void onAudioSessionId(int audioSessionId) {
            // Do nothing.
        }

        @Override
        public void onAudioDecoderInitialized(String decoderName, long initializedTimestampMs, long initializationDurationMs) {
            // Do nothing.
        }

        @Override
        public void onAudioInputFormatChanged(Format format) {
            // Do nothing.
        }

        @Override
        public void onAudioSinkUnderrun(int bufferSize, long bufferSizeMs, long elapsedSinceLastFeedMs) {
            // Do nothing.
        }

        @Override
        public void onAudioDisabled(DecoderCounters counters) {
            // Do nothing.
            Log.d(TAG, "onAudioDisabled: ");
        }
    }


        public static int getResId(String resName, Class<?> c) {

        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Creates an instance of the CronetEngine class.
     * Instances of CronetEngine require a lot of resources. Additionally, their creation is slow
     * and expensive. It's recommended to delay the creation of CronetEngine instances until they
     * are required and reuse them as much as possible.
     * @return An instance of CronetEngine.
     */
    private synchronized CronetEngine getCronetEngine() {
        if(cronetEngine == null) {
            CronetEngine.Builder myBuilder = new CronetEngine.Builder(this);
            cronetEngine = myBuilder.build();
        }
        return cronetEngine;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.microphone_button:
                Log.d(TAG, "onClick microphon");
//                recordAndASR(view);
                break;

            case R.id.start_button:
                sessionId = UUID.randomUUID().toString();
                getDialogResponse("LaunchIntent", Collections.singletonMap("session_id", sessionId));
                break;

            case R.id.yes_button:
                getDialogResponse("ans-positive", Collections.singletonMap("session_id", sessionId));
                break;

            case R.id.correct_button:
                getDialogResponse("animal-spanish",
                                  new HashMap<String, String>() {{
                                      put("session_id", sessionId);
                                      put("animal-spanish", imageView.getContentDescription().toString());
                                  }});
                break;
            case R.id.wrong_button:
                getDialogResponse("animal-spanish",
                                  new HashMap<String, String>() {{
                                      put("session_id", sessionId);
                                      put("animal-spanish", "");
                                  }});
                break;
        }
    }

//    private void recordAndASR(View view) {
//        Log.d(TAG, "recordAndASR: ");
//
//        ImageButton microphoneButton = (ImageButton)view;
//        if (RecordingHelper.getInstance().hasRequiredPermissions(getApplicationContext())) {
//            if (!RecordingHelper.getInstance().isRecording()) {
//                RecordingHelper.getInstance().startRecording(new RecordingHelper.RecordingListener() {
//                    @Override
//                    public void onRecordingSucceeded(File output) {
//                        Log.d(TAG, "onRecordingSucceeded: ");
//                        String path = output.getAbsolutePath();
//                        Log.d(TAG, "path: " + path);
//
//                        try {
//                            FileInputStream fileInputStream = new FileInputStream(output);
//                            mSpeechService.recognizeInputStream(fileInputStream);
//
//                        } catch (FileNotFoundException e) {
//                            Log.e(TAG, e.getLocalizedMessage());
//                        }
//
//                        microphoneButton.setImageDrawable(getDrawable(R.drawable.ic_baseline_mic_none_24px));
//
//                    }
//
//                    @Override
//                    public void onRecordingFailed(Exception e) {
//                        Log.d(TAG, "onRecordingFailed: " + e);
//                    }
//                }, new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date()));
//
//                microphoneButton.setImageDrawable(getDrawable(R.drawable.ic_baseline_mic_24px));
//
//            } else {
//                RecordingHelper.getInstance().stopRecording();
//
//            }
//        } else {
//            RecordingHelper.getInstance().requestRequiredPermissions(this);
//        }
//    }

//    private void startListening() {
//        if (RecordingHelper.getInstance().hasRequiredPermissions(getApplicationContext())) {
//            if (!RecordingHelper.getInstance().isRecording()) {
//                RecordingHelper.getInstance().startRecording(new RecordingHelper.RecordingListener() {
//                    @Override
//                    public void onRecordingSucceeded(File output) {
//                        Log.d(TAG, "onRecordingSucceeded: ");
//                        String path = output.getAbsolutePath();
//                        Log.d(TAG, "path: " + path);
//
//                        try {
//                            FileInputStream fileInputStream = new FileInputStream(output);
//                            mSpeechService.recognizeInputStream(fileInputStream);
//
//                        } catch (FileNotFoundException e) {
//                            Log.e(TAG, e.getLocalizedMessage());
//                        }
//
//                        microphoneButton.setImageDrawable(getDrawable(R.drawable.ic_baseline_mic_none_24px));
//
//                    }
//
//                    @Override
//                    public void onRecordingFailed(Exception e) {
//                        Log.d(TAG, "onRecordingFailed: " + e);
//                    }
//                }, new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date()));
//
//                microphoneButton.setImageDrawable(getDrawable(R.drawable.ic_baseline_mic_24px));
//
//            } else {
//                RecordingHelper.getInstance().stopRecording();
//
//            }
//        } else {
//            RecordingHelper.getInstance().requestRequiredPermissions(this);
//        }
//    }

    private void startVideoRecording() {
        if (BackgroundVideoRecorder.hasRequiredPermissions(this)){
            videoRecordIntent = new Intent(this, BackgroundVideoRecorder.class);
//        videoRecordIntent.putExtra("actiontype",BackGroundVideoRecorder.ACTION_TYPE_FRONT_CAMERA_RECORDING);
            startService(videoRecordIntent);
        } else {
            BackgroundVideoRecorder.requestRequiredPermissions(this);
        }
    }

    private void stopRecording() {
        stopService(videoRecordIntent);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        switch (requestCode) {
//            case REQUEST_OPEN_DOCUMENT:
//                if (resultCode == RESULT_OK) {
//                    Uri uri = null;
//                    if (data != null) {
//                        uri = data.getData();
//                        Log.d(TAG, "uri: " + uri.getPath());
//                        getContentResolver().takePersistableUriPermission(uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//
//                        alterDocument(uri);
//
//                    }
//                }
//        }
//    }

//    private void alterDocument(Uri uri) {
//        try {
//            ParcelFileDescriptor pfd = getContentResolver().
//                    openFileDescriptor(uri, "w");
//            FileOutputStream fileOutputStream =
//                    new FileOutputStream(pfd.getFileDescriptor());
//            fileOutputStream.write(("Overwritten by MyCloud at " +
//                    System.currentTimeMillis() + "\n").getBytes());
//            // Let the document provider know you're done by closing the stream.
//            fileOutputStream.close();
//            pfd.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
