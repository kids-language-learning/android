package io.cafeliang.kidslanguage.dialogmanager;

import java.util.Map;

public class DialogRequest {
    private final String intent;
    private final Map<String, String> parameters;


    public DialogRequest(String intent, Map<String, String> parameters) {
        this.intent = intent;
        this.parameters = parameters;
    }
}
