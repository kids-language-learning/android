package io.cafeliang.kidslanguage.nlu;

public class NluRequest {
    QueryInput queryInput;

    public NluRequest(String text, String languageCode) {
        this.queryInput = new QueryInput(text, languageCode);
    }

    public String getText() {
        return queryInput.getText().getText();
    }

    public String getLanguageCode() {
        return queryInput.getText().getLanguageCode();
    }
}
