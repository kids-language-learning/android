package io.cafeliang.kidslanguage.nlu;

class QueryInputText {
    String text;
    String languageCode;

    public QueryInputText(String text, String languageCode) {
        this.text = text;
        this.languageCode = languageCode;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }
}
