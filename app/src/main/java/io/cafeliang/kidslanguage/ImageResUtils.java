package io.cafeliang.kidslanguage;

public class ImageResUtils {

    public static int getNormalImageRes(String imageKey){
        if (imageKey.equals("duck")) {
            return R.drawable.duck;
        } else if (imageKey.equals("rabbit")) {
            return R.raw.rabbit;
        } else if (imageKey.equals("fish")) {
            return R.raw.fish;
        } else if (imageKey.equals("horse")) {
            return R.raw.horse;
        } else if (imageKey.equals("sheep")) {
            return R.drawable.sheep;
        } else if (imageKey.equals("dog")) {
            return R.raw.dog;
        }

        return R.drawable.zoo;
    }
}
